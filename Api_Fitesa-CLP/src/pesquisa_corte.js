const Sequelize = require('sequelize')
const { QueryTypes } = require('sequelize')

const configTrf = require('./config/database.trf.js')
const trfConn = new Sequelize(configTrf)

const configProducao = require('./config/database.Producao.js')
const producaoConn = new Sequelize(configProducao)

const configFitImp = require('./config/database.impressao.js')
const FitImpConn = new Sequelize(configFitImp)

const moment = require('moment')

console.log(moment().format('YYYY-MM-DD HH:mm'))

// ///////////////////////////////////////////////////////
// //////////////////////////////////////////////////////

module.exports = {
  async insert () {
    // const Pesquisa = new Promise(async (resolve, reject) => {
    let UltDoffid
    // let timeInsert
    let resultsz3
    console.time('timeInsert')
    try {
      await Promise.all([trfConn.authenticate()]) // se não consegue conectar com o DB retorna um valor para a tag do CLP
    } catch (error) {
      console.log('erro db torfresma')
      return 1
    }
    try {
      await Promise.all([producaoConn.authenticate()]) // se não consegue conectar com o DB retorna um valor para a tag do CLP
    } catch (error) {
      console.log('erro db producao')
      return 2
    }
    try {
      await Promise.all([FitImpConn.authenticate()]) // se não consegue conectar com o DB retorna um valor para a tag do CLP
    } catch (error) {
      console.log('erro db etiquetas')
      return 3
    }

    try {
      // await Promise.all([trfConn.authenticate(), producaoConn.authenticate(), FitImpConn.authenticate()])
      // console.log('Trf connected')
      // console.log('trfbk connected')
      // console.log('FitImp connected')

      // SÓ PARA TESTE
      // await producaoConn.query(`update [SZ2100] set Z2_ACNSTAT= 'SND' where Z2_CORT_ID=97747`, { type: QueryTypes.SELECT })  // DELETAR ESTA LINHA

      // const result = await producaoConn.query("SELECT TOP 1 convert(int,[Z2_DIAMETR]) as [Diameter], convert(int,[Z2_COMLINE]) as [Length], convert(int,[Z2_LARGCOR]) as [DoffWidth],[Z2_FILIAL] as [Location], [Z2_RECURSO] as [MachineID], convert(int,[Z2_ROLCORT]) as [NumberOfRibbon],[Z2_GRAMAT] as [Gramage], convert(int,[Z2_NUCLEO]) as [CoreID], CONVERT ( bit,[Z2_ROBOT]) as [UseRobot], [Z2_COR] as [ColorID], [Z2_SEQCORT] as [CutSequenceID], [Z2_ACNPROC] as SchedProdDate, [Z2_ERPCORT] as [ErpDoffID], [Z2_ERPEMPR] as [ErpCompanyName], [Z2_ERPCNPJ] as [ErpCountry], [Z2_ERPNUM] as ErpTroNum, [Z2_ERPFAM] as [ErpFamily],[Z2_ERPTECN] as [ErpTecnology],[Z2_ERPADT] as [ErpAdditive],[Z2_ACNSTAT] as [AcnStatus],[Z2_ACNMSG] as [AcnMsg],[Z2_ACNCMSG] as [AcnMsgCode], [Z2_STATUS] as [DoffStatus], [Z2_DESCCOR] as [DoffDescr] FROM [SZ2100] WHERE [Z2_FILIAL]= 33 AND [Z2_ACNSTAT] = 'SND' AND Z2_ERPNUM = 3", { type: QueryTypes.SELECT })
      const result = await producaoConn.query(`SELECT TOP 1 convert(int,[Z2_DIAMETR]) as [Diameter], convert(int,[Z2_COMLINE]) as [Length], convert(int,[Z2_LARGCOR]) as [DoffWidth],[Z2_FILIAL] as [Location], [Z2_RECURSO] as [MachineID], convert(int,[Z2_ROLCORT]) as [NumberOfRibbon],[Z2_GRAMAT] as [Gramage], convert(int,[Z2_NUCLEO]) as [CoreID], CONVERT ( bit,[Z2_ROBOT]) as [UseRobot], [Z2_COR] as [ColorID], [Z2_SEQCORT] as [CutSequenceID],  [Z2_DTPROG] as SchedProdDate, [Z2_ERPCORT] as [ErpDoffID], [Z2_ERPEMPR] as [ErpCompanyName], [Z2_ERPCNPJ] as [ErpCountry],[Z2_ERPNUM] as [ErpTroNum], [Z2_ERPFAM] as [ErpFamily],[Z2_ERPTECN] as [ErpTecnology],[Z2_ERPADT] as [ErpAdditive],[Z2_ACNSTAT] as [AcnStatus],[Z2_ACNMSG] as [AcnMsg],[Z2_ACNCMSG] as [AcnMsgCode], convert(varchar(3),(convert (int,[Z2_STATUS]))) as [DoffStatus], [Z2_DESCCOR] as [DoffDescr]  FROM [SZ2100] WHERE ( [Z2_FILIAL]= 33 AND [Z2_ACNSTAT] = 'SND' AND Z2_ERPNUM = 3)`,{ type: QueryTypes.SELECT })
      console.log(result)
      if (result.length) {
        if (result[0].Diameter >= 600 && result[0].Diameter <= 1500 && result[0].ErpDoffID && result[0].NumberOfRibbon >= 1 && result[0].NumberOfRibbon <= 70 && result[0].Gramage > 7 && result[0].Gramage <= 70) {
          resultsz3 = await producaoConn.query(`SELECT Z3_FILIAL as Location, convert(int,Z3_POSBOB) as CutNumber, convert(int,Z3_LARGBOB) as RibbonWidth, Z3_PESOBOB as RibbonWeight, convert(int,Z3_M2BOB) as RibbonSqm, Z3_BUND_ID as StackableID, CONVERT (bit,Z3_ROBOT) as UseRobot, convert(int,Z3_DEFEITO) as DefectPresent, convert(int,Z3_PRGEMB) as StackWrappingType,  convert(bit,Z3_FILME) as UseFilm,  convert(bit,Z3_PALLET) as UsePallet, convert(bit,Z3_DISCSUP) as UseHeader, convert(bit,Z3_DISCINF) as UseFooter, convert(int,Z3_IMPTUB) as LabelInternal, convert(int,Z3_ETQTUB) as LabelInternalID, convert(int,Z3_IMPTUB2) as LabelExternal, convert(int,Z3_ETQTUB2) as LabelExternalID, convert(int,Z3_IMPPCT) as LabelBundle, convert(int,[Z3_ETQPCT]) as LabelBundleID, convert(int,Z3_STATUS) as Status, convert(int,Z3_PROCRXS) as ProcessedRxS, convert(int,Z3_RXS0) as RxS0, convert(int,Z3_RXS1) as RxS1, convert(int,Z3_PILHA) as StackCode, Z3_ERPCORT as ErpDoffID, Z3_ERPBOB as ErpRollID, Z3_ERPPCT as ErpBundleID, Z3_ERPOP as ErpProdOrdNum, Z3_ERPLOTE as ErpLotNum, Z3_ERPSLOT as ErpSubLotNum, Z3_ERPCPRO as ErpProdCode, Z3_ERPDPRO as ErpProdDescr, Z3_ERPPRCL as ErpFinalCustProdCode, Z3_ERPPOCL as ErpFinalCustPurchOrder, Z3_ERPPLT as ErpPalletCode, [Z3_ERPPV] as ErpSalesOrder, Z3_ERPITPV as ErpItemSalesOrder, Z3_ERPTURM as ErpShift, [Z3_ERPNCOM] as ErpTradeName, convert(int,Z3_ERPLTEN) as ErpTensionedWidth, Z3_ERPPRFX as ErpPrfx from [SZ3100]  WHERE [Z3_FILIAL]= 33 AND [Z3_ERPCORT] = '${result[0].ErpDoffID}'  ORDER BY [Z3_POSBOB] ASC`, { type: QueryTypes.SELECT })
          console.log(resultsz3)
          if (resultsz3.length) {
            console.log('suceso22')
            let cont = 0
            for (let i = 0; i < result[0].NumberOfRibbon; i++) {
              if (resultsz3[i].UseRobot === true && resultsz3[i].RibbonWidth <= 1000 && resultsz3[i].RibbonWeight <= 360.000) {
                cont = 1
              } else {
                cont = 2
              }
            } // fecha for
            console.log('contador', cont)

            if (cont === 1) {
              /// atualização da tabela sz2////////////
              await trfConn.query(`INSERT INTO TRFLog_SZ2 (Diameter, Length, DoffWidth, Location, MachineID,  NumberOfRibbon, Gramage, CoreID, UseRobot, ColorID,  CutSequenceID, SchedProdDate, ErpDoffID, ErpCompanyName, ErpCountry, ErpTroNum, ErpFamily, ErpTecnology, ErpAdditive, AcnStatus, AcnMsg, AcnMsgCode, DoffStatus, DoffDescr, OrderType) 
              VALUES (${result[0].Diameter}, ${result[0].Length}, ${result[0].DoffWidth}, ${result[0].Location}, '${result[0].MachineID.trim()}',  ${result[0].NumberOfRibbon}, ${result[0].Gramage}, ${result[0].CoreID}, ${result[0].UseRobot ? 1 : 0}, '${result[0].ColorID.trim() || null}',  '${result[0].CutSequenceID.trim()}', ${moment(result[0].SchedProdDate).format('YYYY-MM-DD')}, '${result[0].ErpDoffID.trim() || null}', '${result[0].ErpCompanyName.trim() || null}', '${result[0].ErpCountry.trim() || null}', ${result[0].ErpTroNum}, '${result[0].ErpFamily.trim() || null}', '${result[0].ErpTecnology.trim()}', '${result[0].ErpAdditive.trim() || null}', 'CHK', '${result[0].AcnMsg.trim() || null}', '${result[0].AcnMsgCode.trim() || null}', '${result[0].DoffStatus}', '${result[0].DoffDescr.trim() || null}', 'AUT')`, { type: QueryTypes.INSERT })
              /// seleção do ultimo DoffID inserido na tabela sz2/////
              UltDoffid = await trfConn.query('SELECT TOP 1 DoffID from TRFLog_SZ2 order by DoffID desc', { type: QueryTypes.SELECT })
              console.log('ultimo doff id', UltDoffid[0].DoffID)

              /// atualização da tabela sz3////////////
              for (let k = 0; k < result[0].NumberOfRibbon; k++) {
                await trfConn.query(`INSERT INTO TRFRibbon_SZ3 ([Location], DoffID, CutNumber,  RibbonWidth ,RibbonWeight, RibbonSqm ,StackableID, UseRobot ,DefectPresent, StackWrappingType ,UseFilm, UsePallet ,UseHeader, UseFooter ,LabelInternal ,LabelInternalID ,LabelExternal ,LabelExternalID ,LabelBundle ,LabelBundleID ,Status ,ProcessedRxS ,RxS0 ,RxS1 , StackCode ,ErpDoffID, ErpRollID  ,ErpBundleID, ErpProdOrdNum  ,ErpLotNum ,ErpSubLotNum ,ErpProdCode, ErpProdDescr ,ErpFinalCustProdCode ,ErpFinalCustPurchOrder ,ErpPalletCode, ErpSalesOrder ,ErpItemSalesOrder ,ErpShift, ErpTradeName ,ErpTensionedWidth, ErpPrfx ,OrderType2) VALUES (${resultsz3[k].Location}, ${UltDoffid[0].DoffID} ,${resultsz3[k].CutNumber}, ${resultsz3[k].RibbonWidth}, ${resultsz3[k].RibbonWeight}, ${resultsz3[k].RibbonSqm}, '${resultsz3[k].StackableID.trim()}', ${resultsz3[k].UseRobot ? 1 : 0}, ${resultsz3[k].DefectPresent ? 1 : 0}, ${resultsz3[k].StackWrappingType}, '${resultsz3[k].UseFilm ? 1 : 0}', '${resultsz3[k].UsePallet ? 1 : 0}', '${resultsz3[k].UseHeader ? 1 : 0}', '${resultsz3[k].UseFooter ? 1 : 0}', ${resultsz3[k].LabelInternal}, ${resultsz3[k].LabelInternalID}, ${resultsz3[k].LabelExternal}, ${resultsz3[k].LabelExternalID}, ${resultsz3[k].LabelBundle}, ${resultsz3[k].LabelBundleID}, ${resultsz3[k].Status}, ${resultsz3[k].ProcessedRxS}, ${resultsz3[k].RxS0}, ${resultsz3[k].RxS1}, ${resultsz3[k].StackCode}, '${resultsz3[k].ErpDoffID.trim()}', '${resultsz3[k].ErpRollID.trim()}', '${resultsz3[k].ErpBundleID.trim()}', '${resultsz3[k].ErpProdOrdNum.trim()}', '${resultsz3[k].ErpLotNum.trim()}', '${resultsz3[k].ErpSubLotNum.trim()}', '${resultsz3[k].ErpProdCode.trim()}', '${resultsz3[k].ErpProdDescr.trim()}', '${resultsz3[k].ErpFinalCustProdCode.trim()}', '${resultsz3[k].ErpFinalCustPurchOrder.trim()}', '${resultsz3[k].ErpPalletCode.trim()}', '${resultsz3[k].ErpSalesOrder.trim()}', '${resultsz3[k].ErpItemSalesOrder.trim()}', '${resultsz3[k].ErpShift.trim()}', '${resultsz3[k].ErpTradeName.trim()}', ${resultsz3[k].ErpTensionedWidth}, '${resultsz3[k].ErpPrfx.trim()}', 'AUT')`, { type: QueryTypes.INSERT })
              }
              // /////// insere na tabela do doff (AHI_CELLI_NORMAL_DOFF) no banco de impressão ////////////
              // await FitImpConn.query(`INSERT INTO AHI_CELLI_NORMAL_DOFF VALUES ('FF', 'FF', 'MACH3', ${Ult_doffid[0].DoffID}, '${result[0].ErpDoffID}', ${parseInt(moment().format('YYYYMMDD'))}, ${parseInt(moment().format('HHmm'))}, ${result[0].NumberOfRibbon}, ${result[0].Gramage}, ${result[0].Length}, ${result[0].Diameter} ,${result[0].CoreID} , GETDATE(), '${result[0].UseRobot ? 1 : 0}', 'CHK', 'Doff ok', '0' )`, { type: QueryTypes.INSERT })
              await FitImpConn.query(`INSERT INTO [AHI_CELLI_NORMAL_DOFF] VALUES ('FF', 'FF', '${result[0].MachineID.trim()}',  ${UltDoffid[0].DoffID}, '${result[0].ErpDoffID.trim()}', ${parseInt(moment().format('YYYYMMDD'))}, ${parseInt(moment().format('HHmm'))}, ${result[0].NumberOfRibbon}, ${result[0].Gramage}, ${result[0].Length}, ${result[0].Diameter} ,${result[0].CoreID}, GETDATE(), '${result[0].UseRobot ? 1 : 0}', 'CHK', 'Doff ok', '0' )`, { type: QueryTypes.INSERT })
              // /// insere na tabela das bobinas no banco de impressão////////////

              const ribbonIdEsp = await trfConn.query(`select * from TRFRibbon_SZ3 where DoffID = ${UltDoffid[0].DoffID}`, { type: QueryTypes.SELECT })
              console.log('ribonnID_esp:', ribbonIdEsp)

              for (let j = 0; j < result[0].NumberOfRibbon; j++) {
                // await FitImpConn.query(`insert into AHI_CELLI_NORMAL_DOFF_ROLL (FACILITY, WAREHOUSE, MACHINE_ID, DOFF_SET_ID, CUT_NUMBER, PACKAGING_PROGRAM) values('ff', 'aa', 'SP3',2323, 33, 1)`, { type: QueryTypes.INSERT })

                await FitImpConn.query(`INSERT INTO [dbo].[AHI_CELLI_NORMAL_DOFF_ROLL] values ('FF', 'FF','${result[0].MachineID.trim()}', ${UltDoffid[0].DoffID}, '${result[0].ErpDoffID.trim()}', '${ribbonIdEsp[j].ErpRollID.trim()}', ${ribbonIdEsp[j].RibbonID}, ${ribbonIdEsp[j].CutNumber}, ${ribbonIdEsp[j].ErpProdOrdNum.trim()}, '${result[0].UseRobot ? 1 : 0}', 
                '${ribbonIdEsp[j].ErpItemSalesOrder}', ${ribbonIdEsp[j].RibbonWidth}, '${ribbonIdEsp[j].DefectPresent ? 1 : 0}' , '${ribbonIdEsp[j].RxS0}', '${ribbonIdEsp[j].RxS1}', ${result[0].Gramage} , ${ribbonIdEsp[j].ErpSalesOrder}, '${result[0].CoreID}', 
                '${ribbonIdEsp[j].ErpLotNum}',  '${ribbonIdEsp[j].DefectPresent ? 1 : 0}', '${ribbonIdEsp[j].UseHeader ? 1 : 0}', '${ribbonIdEsp[j].UseFooter ? 1 : 0}', ${ribbonIdEsp[j].LabelBundle}, ${ribbonIdEsp[j].stackWrappingType} )`, { type: QueryTypes.INSERT })
              } // Fecha for

              // /// atualização do status da tabela sz2 do cliente, corte ok///////
              await producaoConn.query("UPDATE SZ2100 set Z2_ACNSTAT = 'CHK' WHERE Z2_ACNSTAT = 'SND' AND Z2_FILIAL= 33 AND Z2_ERPNUM = 3", { type: QueryTypes.UPDATE })
            } else {
              // /// atualização do status da tabela sz2 do cliente, não pode ser procesado o corte///////
              await producaoConn.query("UPDATE SZ2100 set Z2_ACNSTAT = 'ERR' WHERE Z2_ACNSTAT = 'SND' AND Z2_FILIAL= 33 AND Z2_ERPNUM = 3", { type: QueryTypes.UPDATE })
            }
          } else {
            console.log('not suceso')
          }
        } else {
          // /// atualização do status da tabela sz2 do cliente, não pode ser procesado o corte////////
          await producaoConn.query("UPDATE SZ2100 set Z2_ACNSTAT = 'ERR' WHERE Z2_ACNSTAT = 'SND' AND Z2_FILIAL= 33 AND Z2_ERPNUM = 3", { type: QueryTypes.UPDATE })
        }
      }

      trfConn.close()
      producaoConn.close()
      FitImpConn.close()
      console.timeEnd('timeInsert')
      return
    } catch (e) {
      console.timeEnd('timeInsert')
      // return (e)
      console.log(e)
    }
  }
}
