const S7Endpoint = require('./s7endpoint.js')
const S7ItemGroup = require('./s7itemGroup.js')
const plcVars = require('./tagconfig/protocol.js')
const Receber = require('../src/ReceberEmb2')
const Enviar = require('../src/EnviarEmb2')
const Pesquisa = require('./pesquisa_corte.js')

// const utilsError = require('./utils/error')
// const { errorHandler, errorResponse } = utilsError

// // Connection to PLC
const plc = new S7Endpoint({ host: '172.173.1.1', rack: 0, slot: 1 })
// //Verificação do status de conexão
// plc.on('error', e => console.log('PLC Error!', e), plc.connect());
// plc.on('disconnect', () => console.log('PLC Disconnect'));

plc.on('connect', async () => {
  try {
    const itemGroup = new S7ItemGroup(plc)
    itemGroup.setTranslationCB(tag => plcVars.plcVars[tag]) // translates a tag name to its address
    itemGroup.addItems(Object.keys(plcVars.plcVars)) // mapeamento das variaveis
    const plcRead = await itemGroup.readAllItems() // leitura de todos as variáveis

    console.log('ReceberEmb2=', plcRead.ReceberEmb2)
    if (plcRead.ReceberEmb2) { Receber.insert() }
    if (plcRead.EnviarEmb2) {
      Enviar.insert()
      plc.disconnect()
    } else {
      const result = Pesquisa.insert()
      if (result === 1) { // nao conectou com banco trf
        await itemGroup.writeItems(['statusDB'], [1])
      } else if (result === 2) { // nao conectou banco produçao
        await itemGroup.writeItems(['statusDB'], [2])
      } else if (result === 3) { // nao conectou banco Etiquetas
        await itemGroup.writeItems(['statusDB'], [3])
      }
      plc.disconnect()
    }
  } catch (error) {
    console.log(error)
  }
})
