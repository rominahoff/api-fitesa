// https://manuel-rauber.com/2015/11/08/connect-to-ms-sql-using-node-js-and-sequelizejs/
// @todo criar config para development e production
module.exports = {
  dialect: 'mssql',
  username: 'Torfresma',
  password: 'Torfresma@123',
  host: '127.0.0.1',
  port: '1433',
  database: 'banco_ej1',
  logging: () => false,
  dialectOptions: {
    options: {
      requestTimeout: 30000,
      encrypt: false,
      useUTC: false // tivemos que setar o UTC como false pois estava retornado os dates - 1 dia,7
    }
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 60000,
    idle: 10000
  }
}
