const Sequelize = require('sequelize')
const { QueryTypes } = require('sequelize')

// const configTrf = require('./config/database.trf.js')
// const trfConn = new Sequelize(configTrf)

const configProducao = require('./config/database.Producao.js')
const producaoConn = new Sequelize(configProducao)

// const configFitImp = require('./config/database.impressao.js')
// const FitImpConn= new Sequelize(configFitImp)

const moment = require('moment')
console.log(moment().format('YYYY-MM-DD HH:mm'))

// ///////////////////////////////////////////////////////
// //////////////////////////////////////////////////////

module.exports = {
  async insert () {
    // const Receber = new Promise(async (resolve, reject) => {

    console.time('timeInsert2')
    try {
      await Promise.all([producaoConn.authenticate()])
      const result2 = await producaoConn.query("SELECT TOP 1 [Z2_ERPCORT] FROM [dbo].[SZ2100] WHERE [Z2_ACNSTAT] ='SND' AND Z2_ERPNUM = 2", { type: QueryTypes.SELECT })
      console.log(result2)
      if (result2.length) {
        await producaoConn.query("UPDATE [dbo].[SZ2100] SET Z2_ERPNUM = 3 WHERE [Z2_ACNSTAT] ='SND' AND Z2_ERPNUM = 2", { type: QueryTypes.UPDATE })
      }
      producaoConn.close()
      console.timeEnd('timeInsert2')
    // return resolve('Finished2!')
    } catch (e) {
      console.timeEnd('timeInsert2')
      // return (e)
      console.log(e)
    }
  }
}
