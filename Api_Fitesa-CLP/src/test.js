const { plc } = require('.')
const S7ItemGroup = require('./s7itemGroup.js')
const plcVars = require('./tagconfig/protocol.js')
const plcVar = plcVars

plc.on('connect', async () => {
// // The S7ItemGroup to perform optimized read/write of variables
  const itemGroup = new S7ItemGroup(plc)
  while (true) {
    itemGroup.setTranslationCB(tag => plcVar.plcVars[tag]) // translates a tag name to its address
    itemGroup.addItems(Object.keys(plcVar.plcVars)) // mapeamento das variaveis
    // await itemGroup.writeItems('L1_Status', [value])//livebit
    const plcRead = await itemGroup.readAllItems() // leitura de todos as variáveis
    console.log(plcRead)
  }
})
