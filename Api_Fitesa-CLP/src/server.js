const cluster = require('cluster')
const numCPUs = require('os').cpus().length

const app = require('./app')

const logger = require('./utils/logger')

if (cluster.isMaster) {
  logger.info(`Master ${process.pid} is running`)
  logger.info(`Number of clusters: ${numCPUs}`)

  // Fork workers.
  for (let i = 0; i < numCPUs; i += 1) cluster.fork()

  // Check if work id is died
  cluster.on('exit', (worker) => logger.info(`worker ${worker.process.pid} died`))
} else {
  // This is Workers can share any TCP connection
  // It will be initialized using express
  logger.info(`Worker ${process.pid} started`)

  app.listen(process.env.PORT || 3000, '0.0.0.0')
}
