const express = require('express')
const cors = require('cors')

const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')

require('./database/trf')

class AppController {
  constructor () {
    this.express = express()
    this.middlewares()
    this.routes()
  }

  middlewares () {
    this.express.use(express.json())
    this.express.use(cors())
    this.express.use(express.json())
    this.express.use(express.urlencoded({ extended: true }))
  }

  routes () {
    // this.express.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
    // this.express.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

    this.express.use('/api', require('./routes'))

    this.express.use((req, res, next) => {
      if (req.url === '/') {
        res.redirect('/api')
        return
      }
      next()
    })
  }
}

module.exports = new AppController().express
