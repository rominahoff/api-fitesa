/* DESCRIÇÃO DO FORMATO DAS VARIAVEIS

TEST1: 'MR4',          // Memory real at MD4
TEST2: 'M32.2',        // Bit at M32.2
TEST3: 'M20.0',        // Bit at M20.0
TEST4: 'DB1,REAL0.20', // Array of 20 values in DB1
TEST5: 'DB1,REAL4',    // Single real value
TEST6: 'DB1,REAL8',    // Another single real value
TEST7: 'DB1,INT12.2',  // Two integer value array
TEST8: 'DB1,LREAL4',   // Single 8-byte real value
TEST9: 'DB1,X14.0',    // Single bit in a data block
TEST10: 'DB1,X14.0.8'  // Array of 8 bits in a data block
*/

/* Mapeamento valores DBs */
module.exports = {
  liveBit: {
    // xLiveBit: 'DB48,X36.1'  // tag bool para  testar a conexão  de algum dispositivo
  },
  plcVars: {
    EnviarEmb2: 'DB167,X678.2', // tag para enviar corte para a embaladora 2 (3->2)
    ReceberEmb2: 'DB167,X678.1', // tag para receber corte da embaladora 2  (2->3)
    // Criar tag no CLP e atualizar pos de memoria
    statusDB: 'DB167,X678.2' // tag escreve no CLP o status da conexão com as DB.
  }
}
