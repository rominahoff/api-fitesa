const Sequelize = require('sequelize')
const { QueryTypes } = require('sequelize')

// const configTrf = require('./config/database.trf.js')
// const trfConn = new Sequelize(configTrf)

const configProducao = require('./config/database.Producao.js')
const producaoConn = new Sequelize(configProducao)

// const configFitImp = require('./config/database.impressao.js')
// const FitImpConn= new Sequelize(configFitImp)

//  ///////////////////////////////////////////////////////
//  //////////////////////////////////////////////////////
module.exports = {
  async insert () {
    // const Enviar = new Promise(async (resolve, reject) => {
    console.time('timeInsert')
    try {
      await Promise.all([producaoConn.authenticate()])

      const result3 = await producaoConn.query("SELECT TOP 1 [Z2_ERPCORT] FROM [dbo].[SZ2100] WHERE [Z2_ACNSTAT] ='SND' AND Z2_ERPNUM = 3", { type: QueryTypes.SELECT })
      console.log(result3)
      if (result3.length) {
        await producaoConn.query("UPDATE [dbo].[SZ2100] SET Z2_ERPNUM = 2 WHERE [Z2_ACNSTAT] ='SND' AND Z2_ERPNUM = 3", { type: QueryTypes.UPDATE })
      }
      producaoConn.close()
      console.timeEnd('timeInsert')
      return
    } catch (e) {
      console.timeEnd('timeInsert')
      // return (e)
      console.log(e)
    }
  }
}
