const logger = require('./logger')

module.exports = {
  errorHandler: (code, msg) => {
    const e = new Error(msg)
    e.code = code
    return e
  },

  errorResponse: (e, res) => {
    logger.error(`${res.req.method} - ${res.req.url} - ${JSON.stringify(res.req.params)} - ${JSON.stringify(res.req.query)} - ${JSON.stringify(res.req.body)} - ${e.message}`)
    const status = parseInt(e.code) ? e.code : 500
    const msg = e.message ? e.message : (e.name ? e.name : 'Internal Error')
    return res.status(status).json({ msg })
  }
}